<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnggotaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('konten');
});

Route::get('data', [AnggotaController::class, 'index']);
Route::get('data/{id}', [AnggotaController::class, 'show']);
Route::get('buat', [AnggotaController::class, 'create']);
Route::post('simpan', [AnggotaController::class, 'store']);
Route::get('edit/{id}', [AnggotaController::class, 'edit']);
Route::post('update/{id}', [AnggotaController::class, 'update']);
Route::get('delete/{id}', [AnggotaController::class, 'destroy']);
