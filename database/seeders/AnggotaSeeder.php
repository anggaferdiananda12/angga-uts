<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Anggota;

class AnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Anggota::create([
            'nama' => 'Angga',
            'alamat' => 'Palengaan',
            'ttl' => '18-18-2021',
            'umur' => '20',
        ]);
    }
}
